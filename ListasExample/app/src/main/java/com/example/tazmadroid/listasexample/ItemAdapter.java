package com.example.tazmadroid.listasexample;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Tazmadroid on 05/11/16.
 */

public class ItemAdapter extends ArrayAdapter<ItemModel> {

  private List<ItemModel> models=null;
  private Context context=null;

  public ItemAdapter(Context context, List<ItemModel> objects) {
    super(context, R.layout.item_state, objects);
    this.context=context;
    this.models=objects;
  }

  @NonNull
  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    LayoutInflater inflater=(LayoutInflater)context.
        getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    View item = inflater.inflate(R.layout.item_state,null);
    TextView stateTextView = (TextView) item.
        findViewById(R.id.stateTextView);
    TextView cityTextView = (TextView) item.
        findViewById(R.id.cityTextView);

    stateTextView.setText(models.get(position).getTitle());
    cityTextView.setText(models.get(position).getSubtitle());

    return item;
  }

  @Nullable
  @Override
  public ItemModel getItem(int position) {
    return models.get(position);
  }

  @Override
  public int getCount() {
    return models.size();
  }
}
