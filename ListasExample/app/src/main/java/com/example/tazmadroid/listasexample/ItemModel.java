package com.example.tazmadroid.listasexample;

/**
 * Created by Tazmadroid on 05/11/16.
 */

public class ItemModel {

  private int id=0;
  private String title=null;
  private String subtitle=null;
  private String icon=null;


  public ItemModel(int id, String title,
                   String subtitle, String icon) {
    this.id = id;
    this.title = title;
    this.subtitle = subtitle;
    this.icon = icon;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getSubtitle() {
    return subtitle;
  }

  public void setSubtitle(String subtitle) {
    this.subtitle = subtitle;
  }

  public String getIcon() {
    return icon;
  }

  public void setIcon(String icon) {
    this.icon = icon;
  }
}
