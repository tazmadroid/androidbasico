package com.example.tazmadroid.listasexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

  private GridView statesListView = null;
  private ItemAdapter statesAdapter=null;
  private List<ItemModel> models=null;
  private Toolbar toolbar=null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    statesListView = (GridView) findViewById(R.id.statesListView);
    initStates();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.main_menu,menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()){
      case R.id.add:
        addModel();
        return true;
      case R.id.remove:
        Log.d("ItemSelected","REMOVE");
        return true;
      case R.id.show:
        Log.d("ItemSelected","SHOW");
        return true;
      case R.id.query:
        Log.d("ItemSelected","QUERY");
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }

  }

  private void initStates(){
    initModels();
    statesAdapter = new
        ItemAdapter(getBaseContext(),models);
    statesListView.setAdapter(statesAdapter);
    statesListView.setNumColumns(2);
    statesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ItemModel state=(ItemModel) statesListView.
            getAdapter().getItem(position);
        Log.d("State",state.getTitle());
      }
    });
  }

  private void addModel(){
    models.add(new ItemModel(32,"Guanajuato","Leon","Gto"));
    statesAdapter.notifyDataSetChanged();
  }

  private void initModels(){
    models = new ArrayList<ItemModel>();
    models.
        add(new ItemModel(0,"Aguascalientes",
            "Ojuelos","Aguas"));
    models.
        add(new ItemModel(1,"Jalisco",
            "Guadalajara","Jal"));
    models.
        add(new ItemModel(2,"Guerrero",
            "Chilpancingo","Gro"));
    models.
        add(new ItemModel(3,"Ciudad de México",
            "Benito Juarez","CdMx"));
    models.
        add(new ItemModel(4,"Yucatan",
            "Merida","Yuc"));
  }
}
