package com.cybertech.filesapp;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity {

  private static final String NAME_FILE = "Prueba.txt";

  private EditText lineEditText = null;
  private Button addLineButton = null;
  private Button addLineSDButton = null;
  private TextView lineTextView = null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    createFile();

    lineEditText = (EditText) findViewById(R.id.lineEditText);
    addLineButton = (Button) findViewById(R.id.addLineButton);
    addLineSDButton = (Button) findViewById(R.id.addLineSDButton);
    lineTextView = (TextView) findViewById(R.id.lineTextView);

    addLineButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        String line = lineEditText.getText().toString();
        writeFile(line);
        lineTextView.setText(readFile());
      }
    });

    addLineSDButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        String line = lineEditText.getText().toString();
        writeFileSD(line);
        lineTextView.setText(readFileSD());
      }
    });
  }


  private void createFile() {
    try {
      OutputStreamWriter fout =
          new OutputStreamWriter(
              openFileOutput(NAME_FILE, Context.MODE_PRIVATE));

      fout.close();
    } catch (Exception ex) {
      Log.e("Ficheros", "Error al escribir fichero a memoria interna");
    }
  }

  private void writeFile(String line) {
    try {
      OutputStreamWriter fout =
          new OutputStreamWriter(
              openFileOutput(NAME_FILE, Context.MODE_APPEND));
      fout.write(line);
      fout.close();
    } catch (Exception ex) {
      Log.e("Ficheros", "Error al escribir fichero a memoria interna");
    }
  }

  private String readFile() {
    String texto = null;
    try {
      BufferedReader fin =
          new BufferedReader(
              new InputStreamReader(
                  openFileInput(NAME_FILE)));

      texto = fin.readLine();
      fin.close();
      return texto;
    } catch (Exception ex) {
      Log.e("Ficheros", "Error al leer fichero desde memoria interna");
      return texto;
    }
  }

  private boolean isAccessibleSD() {
    boolean isAvailable = false;
    boolean isWritable = false;
    String state = Environment.getExternalStorageState();
    if (state.equals(Environment.MEDIA_MOUNTED)) {
      isAvailable = true;
      isWritable = true;
    } else if (state.equals(Environment.MEDIA_MOUNTED_READ_ONLY)) {
      isAvailable = true;
      isWritable = false;
    } else {
      isAvailable = false;
      isWritable = false;
    }

    if (isAvailable && isWritable)
      return true;
    else
      return false;
  }

  private void createFileSD() {
    try {
      if (isAccessibleSD()) {
        File ruta_sd = Environment.getExternalStorageDirectory();
        File f = new File(ruta_sd, NAME_FILE);

        OutputStreamWriter fout =
            new OutputStreamWriter(
                new FileOutputStream(f));

        fout.close();
      }
    } catch (Exception ex) {
      Log.e("Ficheros", "Error al escribir fichero a tarjeta SD");
    }
  }

  private void writeFileSD(String line) {
    try {
      if (isAccessibleSD()) {
        File ruta_sd = Environment.getExternalStorageDirectory();
        File f = new File(ruta_sd, NAME_FILE);

        if (!f.exists()) {
          createFileSD();
        }

        OutputStreamWriter fout =
            new OutputStreamWriter(
                new FileOutputStream(f));

        fout.write(line);
        fout.close();

      }
    } catch (Exception ex) {
      Log.e("Ficheros", "Error al escribir fichero a tarjeta SD");
    }
  }

  private String readFileSD() {
    String texto = null;
    try {
      File ruta_sd = Environment.getExternalStorageDirectory();
      File f = new File(ruta_sd,NAME_FILE);
      BufferedReader fin =
          new BufferedReader(
              new InputStreamReader(
                  new FileInputStream(f)));

      texto = fin.readLine();
      fin.close();
      return texto;
    } catch (Exception ex) {
      Log.e("Ficheros", "Error al leer fichero desde tarjeta SD");
      return texto;
    }
  }
}
