package com.cybertech.examplelist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebViewActivity extends AppCompatActivity {

    private WebView myChromeWebView=null;
    private String url="http://www.google.com.mx";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        myChromeWebView = (WebView) findViewById(R.id.myChromeWebView);
        myChromeWebView.getSettings().setJavaScriptEnabled(true);
        myChromeWebView.setWebChromeClient(new WebChromeClient());
        myChromeWebView.setWebViewClient(new MyWebClient());
        myChromeWebView.loadUrl(url);
    }
}
