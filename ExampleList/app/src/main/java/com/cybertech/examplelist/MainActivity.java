package com.cybertech.examplelist;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView personasListView=null;
    private List<Persona> personas=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar= (Toolbar) findViewById(R.id.mainToolbar);
        setSupportActionBar(toolbar);
        personasListView =(ListView) findViewById(R.id.personasListView);
        personas=new ArrayList<Persona>();
        personas.add(new Persona("Jaive","Torres","jtpcybertron85@gmail.com"));
        personas.add(new Persona("Jaive1","Torres","jtpcybertron85@gmail.com"));
        personas.add(new Persona("Jaive2","Torres","jtpcybertron85@gmail.com"));
        personas.add(new Persona("Jaive3","Torres","jtpcybertron85@gmail.com"));
        personas.add(new Persona("Jaive4","Torres","jtpcybertron85@gmail.com"));
        personas.add(new Persona("Jaive5","Torres","jtpcybertron85@gmail.com"));
        personas.add(new Persona("Jaive6","Torres","jtpcybertron85@gmail.com"));
        personas.add(new Persona("Jaive7","Torres","jtpcybertron85@gmail.com"));
        personas.add(new Persona("Jaive8","Torres","jtpcybertron85@gmail.com"));
        personas.add(new Persona("Jaive9","Torres","jtpcybertron85@gmail.com"));
        personas.add(new Persona("Jaive10","Torres","jtpcybertron85@gmail.com"));
        personas.add(new Persona("Jaive11","Torres","jtpcybertron85@gmail.com"));
        personas.add(new Persona("Jaive12","Torres","jtpcybertron85@gmail.com"));
        personas.add(new Persona("Jaive13","Torres","jtpcybertron85@gmail.com"));
        personas.add(new Persona("Jaive14","Torres","jtpcybertron85@gmail.com"));
        personas.add(new Persona("Jaive15","Torres","jtpcybertron85@gmail.com"));
        personas.add(new Persona("Jaive16","Torres","jtpcybertron85@gmail.com"));
        personas.add(new Persona("Jaive17","Torres","jtpcybertron85@gmail.com"));
        personas.add(new Persona("Jaive18","Torres","jtpcybertron85@gmail.com"));
        personas.add(new Persona("Jaive19","Torres","jtpcybertron85@gmail.com"));
        personas.add(new Persona("Jaive20","Torres","jtpcybertron85@gmail.com"));
        PersonaAdapter personaAdapter = new PersonaAdapter(getBaseContext(),personas);
        personasListView.setAdapter(personaAdapter);
        personasListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Persona persona =((PersonaAdapter)adapterView.getAdapter()).getItem(i);
                Log.d("[ESTADO]",persona.toString());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.add:
                agregarPersona();
                return true;
            case R.id.remove:
                removerPersona();
                return true;
            case R.id.show:
                Intent webActivity= new Intent(MainActivity.this,WebViewActivity.class);
                startActivity(webActivity);
                return true;
            case R.id.edit_query:
                Toast.makeText(getBaseContext(),"CONSULTAR",Toast.LENGTH_SHORT).show();
                return true;
            case R.id.removeAll:
                Toast.makeText(getBaseContext(),"REMOVER TODO",Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void agregarPersona(){
        personas.add(new Persona("Jaive"+personas.size()+1,"Torres Pineda","correo@gmail.com"));
        ((PersonaAdapter)personasListView.getAdapter()).notifyDataSetChanged();
    }

    public void removerPersona(){
        personas.remove(personas.size()-1);
        ((PersonaAdapter)personasListView.getAdapter()).notifyDataSetChanged();
    }
}
