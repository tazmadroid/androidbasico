package com.cybertech.examplelist;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by ANDROID on 10/03/2017.
 */

public class PersonaAdapter extends ArrayAdapter<Persona> {

    private Context context=null;
    private List<Persona> personas=null;

    public PersonaAdapter(Context context, List<Persona> objects) {
        super(context, R.layout.item_persona, objects);
        this.context=context;
        this.personas=objects;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;

        if(view==null){
            LayoutInflater inflater = (LayoutInflater) context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view=inflater.inflate(R.layout.item_persona,null);
            holder = new ViewHolder();
            holder.nameTextView = (TextView) view.findViewById(R.id.nameTextView);
            holder.lastNameTextView = (TextView) view.findViewById(R.id.lastNameTextView);
            holder.emailTextView= (TextView) view.findViewById(R.id.emailTextView);
            view.setTag(holder);
        }else{
            holder=(ViewHolder) view.getTag();
        }

        holder.nameTextView.setText(personas.get(position).getName());
        holder.lastNameTextView.setText(personas.get(position).getLastName());
        holder.emailTextView.setText(personas.get(position).getEmail());

        return view;
    }

    @Nullable
    @Override
    public Persona getItem(int position) {
        return personas.get(position);
    }

    @Override
    public int getCount() {
        return personas.size();
    }

    static class ViewHolder{
        TextView nameTextView;
        TextView lastNameTextView;
        TextView emailTextView;
    }
}
