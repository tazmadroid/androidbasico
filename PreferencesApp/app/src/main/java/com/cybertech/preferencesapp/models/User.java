package com.cybertech.preferencesapp.models;

/**
 * BaseDatosApp
 * com.cybertech.basedatosapp.model
 * <p/>
 * Creado por:I.S.C. Jaive Torres Pineda .
 * Descripción:
 * Fecha de creación: 11/11/16.
 * Última actualización:11/11/16.
 * Actualizado por:
 */
public class User {

  private int id=0;
  private String nombre=null;
  private String lastName=null;
  private String birthday=null;
  private String age=null;
  private String sex=null;

  public User() {
  }

  public User(int id, String nombre, String lastName, String birthday,
              String age, String sex) {
    this.id = id;
    this.nombre = nombre;
    this.lastName = lastName;
    this.birthday = birthday;
    this.age = age;
    this.sex = sex;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getBirthday() {
    return birthday;
  }

  public void setBirthday(String birthday) {
    this.birthday = birthday;
  }

  public String getAge() {
    return age;
  }

  public void setAge(String age) {
    this.age = age;
  }

  public String getSex() {
    return sex;
  }

  public void setSex(String sex) {
    this.sex = sex;
  }
}
