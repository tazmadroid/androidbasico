package com.cybertech.preferencesapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.cybertech.preferencesapp.models.User;

public class MainActivity extends AppCompatActivity {

  private TextInputLayout nameTextInputLayout = null;
  private TextInputLayout lastNameTextInputLayout = null;
  private TextInputLayout birthdayTextInputLayout = null;
  private TextInputLayout ageTextInputLayout = null;
  private TextInputLayout sexTextInputLayout = null;
  private Button saveButton = null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    SharedPreferences preferences = getPreferences(MODE_PRIVATE);

    nameTextInputLayout = (TextInputLayout) findViewById(R.id.nameTextInputLayout);
    lastNameTextInputLayout = (TextInputLayout) findViewById(R.id.lastnameTextInputLayout);
    birthdayTextInputLayout = (TextInputLayout) findViewById(R.id.birthdayTextInputLayout);
    ageTextInputLayout = (TextInputLayout) findViewById(R.id.ageTextInputLayout);
    sexTextInputLayout = (TextInputLayout) findViewById(R.id.sexTextInputLayout);
    saveButton = (Button) findViewById(R.id.saveButton);

    nameTextInputLayout.setErrorEnabled(true);
    lastNameTextInputLayout.setErrorEnabled(true);
    birthdayTextInputLayout.setErrorEnabled(true);
    ageTextInputLayout.setErrorEnabled(true);
    sexTextInputLayout.setErrorEnabled(true);

    saveButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        String name = nameTextInputLayout.getEditText().getText().toString();
        String lastName = lastNameTextInputLayout.getEditText().getText().toString();
        String birthday = birthdayTextInputLayout.getEditText().getText().toString();
        String age = ageTextInputLayout.getEditText().getText().toString();
        String sex = sexTextInputLayout.getEditText().getText().toString();

        if (name.isEmpty()) {
          nameTextInputLayout.setError("El nombre no es válido");
        } else {
          nameTextInputLayout.setError(null);
        }

        if (lastName.isEmpty()) {
          lastNameTextInputLayout.setError("El nombre no es válido");
        } else {
          lastNameTextInputLayout.setError(null);
        }
        User user = new User(name.hashCode(), name, lastName, birthday, age, sex);
        saveUser(user);
        Toast.makeText(getBaseContext(), "El usuario se registro", Toast.LENGTH_SHORT).show();
      }
    });
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.activity_main,menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()){
      case R.id.settings:
        Intent settingsIntent = new Intent(MainActivity.this,SettingsActivity.class);
        startActivity(settingsIntent);
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  private void saveUser(User user) {
    SharedPreferences preferences = getSharedPreferences("UsersPreferences", MODE_PRIVATE);
    SharedPreferences.Editor editor = preferences.edit();
    editor.putInt("id", user.getId());
    editor.putString("name", user.getNombre());
    editor.putString("lastname", user.getLastName());
    editor.putString("birthday", user.getBirthday());
    editor.putString("age", user.getAge());
    editor.putString("sex", user.getSex());
    editor.commit();
  }
}
