package com.cybertech.preferencesapp;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;

import java.util.List;

/**
 * PreferencesApp
 * com.cybertech.preferencesapp
 * <p/>
 * Creado por:I.S.C. Jaive Torres Pineda .
 * Descripción:
 * Fecha de creación: 16/11/16.
 * Última actualización:16/11/16.
 * Actualizado por:
 */
public class SettingsActivity extends PreferenceActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

  }

  @Override
  public void onBuildHeaders(List<Header> target) {
    loadHeadersFromResource(R.xml.pref_headers,target);
  }

  @Override
  protected boolean isValidFragment(String fragmentName) {
    return GeneralFragment.class.getName().equals(fragmentName) ||
        NotificationFragment.class.getName().equals(fragmentName)
        || DataSyncFragment.class.getName().equals(fragmentName);
  }

  public static class GeneralFragment extends PreferenceFragment {


    public GeneralFragment() {
      // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      addPreferencesFromResource(R.xml.pref_general);
    }


  }

  public static class DataSyncFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      addPreferencesFromResource(R.xml.pref_data_sync);
    }
  }

  public static class NotificationFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      addPreferencesFromResource(R.xml.pref_notification);
    }
  }
}
