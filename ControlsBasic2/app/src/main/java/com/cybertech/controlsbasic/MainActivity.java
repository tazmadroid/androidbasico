package com.cybertech.controlsbasic;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.cybertech.controlsbasic.bd.BDHelper;
import com.cybertech.controlsbasic.models.User;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

  EditText nameEditText=null;
  EditText lastNameEditText=null;
  EditText addressEditText=null;
  EditText ageEditText=null;
  EditText countryEditText=null;
  EditText emailEditText=null;
  EditText numberPhoneEditText=null;
  Spinner stateSpinner=null;
  RadioGroup sexRadioGroup=null;
  CheckBox musicCheckBox=null;
  CheckBox magazineCheckBox=null;
  CheckBox movieCheckBox=null;
  CheckBox booksCheckBox=null;
  Button initSessionButton=null;

  private BDHelper bdHelper=null;

  private char sex;
  private List<String> intereses=null;
  private String state=null;


  @Override
  protected void onStart() {
    super.onStart();
    Log.d("start","activity 1");
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    Log.d("create","Activity 1");
    //instance BD
    bdHelper = new BDHelper(getBaseContext());

    nameEditText = (EditText) findViewById(R.id.nameEditText);
    lastNameEditText = (EditText) findViewById(R.id.lastNameEditText);
    addressEditText = (EditText) findViewById(R.id.addressEditText);
    ageEditText = (EditText) findViewById(R.id.ageEditText);
    countryEditText = (EditText) findViewById(R.id.countryEditText);
    emailEditText = (EditText) findViewById(R.id.emailEditText);
    numberPhoneEditText = (EditText) findViewById(R.id.numberPhoneEditText);
    stateSpinner = (Spinner) findViewById(R.id.stateSpinner);
    sexRadioGroup = (RadioGroup) findViewById(R.id.sexRadiogroup);
    musicCheckBox = (CheckBox) findViewById(R.id.musicCheckBox);
    movieCheckBox = (CheckBox) findViewById(R.id.musicCheckBox);
    magazineCheckBox = (CheckBox) findViewById(R.id.magazinesCheckBox);
    booksCheckBox = (CheckBox) findViewById(R.id.booksCheckBox);
    initSessionButton = (Button) findViewById(R.id.initSessionButton);
    initStateSpinner();
    initSexRadioGroup();
    initInteres();
    initializeInitSessionButton();
  }

  @Override
  protected void onPause() {
    super.onPause();
    Log.d("pause","activity 1");
  }

  @Override
  protected void onResume() {
    super.onResume();
    Log.d("onResume","activity 1");
  }

  @Override
  protected void onStop() {
    super.onStop();
    Log.d("stop","activity 1");
  }

  @Override
  protected void onDestroy() {
    Log.d("destroy","activity 1");
    bdHelper.close();
    super.onDestroy();
  }

  private void initStateSpinner(){
    final ArrayAdapter<CharSequence> stateAdapter = ArrayAdapter.
        createFromResource(getBaseContext(),R.array.state,
            android.R.layout.simple_spinner_dropdown_item);
    stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    stateSpinner.setAdapter(stateAdapter);
    stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        state=stateAdapter.getItem(position).toString();
      }

      @Override
      public void onNothingSelected(AdapterView<?> parent) {

      }
    });
  }

  private void initSexRadioGroup(){
    sexRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId){
          case R.id.maleRadioButton:
            sex='m';
            break;
          case R.id.femaleRadioButton:
            sex='f';
            break;
          default:
            sex='m';
            break;
        }
      }
    });
  }

  private void initInteres(){
    intereses = new ArrayList<String>();
    musicCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked)
          intereses.add("Musica");
        else{
          if(intereses.contains("Musica"))
            intereses.remove("Musica");
        }
      }
    });
    magazineCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked)
          intereses.add("Revista");
        else{
          if(intereses.contains("Revista"))
            intereses.remove("Revista");
        }
      }
    });
    movieCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked)
          intereses.add("Pelicula");
        else{
          if(intereses.contains("Pelicula"))
            intereses.remove("Pelicula");
        }
      }
    });
    booksCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked)
          intereses.add("Libros");
        else{
          if(intereses.contains("Libros"))
            intereses.remove("Libros");
        }
      }
    });
  }

  private void initializeInitSessionButton(){
    initSessionButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        String name = nameEditText.getText().toString();
        String lastName = lastNameEditText.getText().toString();

        User user = new User(12,name,lastName,"26/11/1985",31,
            Character.toString(sex));
//        if(bdHelper.insert(user))
//          Toast.makeText(getBaseContext(),"Registro insertado",
//              Toast.LENGTH_SHORT).show();
//        else
//          Toast.makeText(getBaseContext(),"Registro no insertado",
//              Toast.LENGTH_SHORT).show();
        saveUser(user);
        Log.d("nombre",name);
        Log.d("apellidos",lastName);
        Log.d("sexo",""+sex);
        Log.d("estado",state);
      }
    });
  }

  private void saveUser(User user){
    SharedPreferences preferences = getSharedPreferences("UserPreferences",MODE_PRIVATE);
    SharedPreferences.Editor editor = preferences.edit();
    editor.putInt("id",user.getId());
    editor.putString("name",user.getName());
    editor.putString("lastname",user.getLastname());
    editor.commit();
    preferences.get
  }
}
