package com.cybertech.controlsbasic;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

/**
 * Created by Tazmadroid on 19/11/16.
 */

public class DialogWarning extends DialogFragment {


  @NonNull
  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    AlertDialog.Builder builder= new AlertDialog.
        Builder(getActivity());
    builder.setTitle("Aviso");
    builder.setMessage("Es una prueba");
    builder.setPositiveButton("OK", new DialogInterface.
        OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        dismiss();
      }
    });
    return builder.create();
  }
}
