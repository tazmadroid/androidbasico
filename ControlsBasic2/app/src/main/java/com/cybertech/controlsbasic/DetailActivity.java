package com.cybertech.controlsbasic;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

  private TextView nameTextView=null;
  private TextView lastNameTextView=null;
  private TextView edadTextView=null;

  @Override
  protected void onStart() {
    super.onStart();
    Log.d("start","activity 2");
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_detail);
    Log.d("create","activity 2");
    Intent detailIntent = getIntent();

    Bundle detailBundle= getIntent().getExtras();

    nameTextView = (TextView) findViewById(R.id.namedetailTextView);
    lastNameTextView = (TextView) findViewById(R.id.lastNameDetailtextView);
    edadTextView = (TextView) findViewById(R.id.edadDetailTextView);

//    String name2 = getIntent().getStringExtra("name");
//    String name= detailBundle.getString("name");
//    String lastName = detailBundle.getString("lastname");
//    char sex = detailBundle.getChar("sex");
//
//    nameTextView.setText(name+" "+name2);
//    lastNameTextView.setText(lastName);
//    edadTextView.setText(""+sex);
  }


  @Override
  protected void onPause() {
    super.onPause();
    Log.d("pause","activity 2");
  }

  @Override
  protected void onResume() {
    super.onResume();
    Log.d("onResume","activity 2");
  }

  @Override
  protected void onStop() {
    super.onStop();
    Log.d("stop","activity 2");
  }

  @Override
  protected void onDestroy() {
    Log.d("destroy","activity 2");
    super.onDestroy();
  }
}
