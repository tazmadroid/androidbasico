package com.cybertech.controlsbasic.bd;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.cybertech.controlsbasic.models.User;

import java.util.HashMap;

/**
 * Created by Tazmadroid on 26/11/16.
 */

public class BDHelper {

  private Context context = null;
  private SQLiteDatabase bd = null;
  private BaseDatos baseDatos = null;

  private static final String NOMBRE_BD = "users_bd";
  private static final String NOMBRE_TABLE_BD = "users";
  private static final int VERSION_BD = 1;

  public BDHelper() {
  }

  public BDHelper(Context context) {
    this.context = context;
    baseDatos = new BaseDatos(context, NOMBRE_BD, null, VERSION_BD);
    bd = baseDatos.getWritableDatabase();
  }

  public boolean insert(User user) {
    boolean insert = false;
    ContentValues insertContentValues = new ContentValues();
    insertContentValues.put("id", user.getId());
    insertContentValues.put("name", user.getName());
    insertContentValues.put("lastname", user.getLastname());
    insertContentValues.put("birthday", user.getBirthday());
    insertContentValues.put("age", user.getAge());
    insertContentValues.put("sex", user.getSex());
    long insertLong = bd.insert(NOMBRE_TABLE_BD, null, insertContentValues);
    if (insertLong != 0 && insertLong>0)
      insert = true;
    else
      insert = false;
    return insert;
  }

  public boolean update(int id, HashMap<String, String> fields){
    if(fields!=null && fields.size()!=0){
      ContentValues contentValues = new ContentValues();
      for(String key : fields.keySet()){
        contentValues.put(key,fields.get(key));
      }
      String[] args = new String[]{Integer.toString(id)};
      long update = bd.update(NOMBRE_TABLE_BD,contentValues,"id=?",args);
      if(update!=0 && update>0){
        return true;
      }else{
        return false;
      }
    }else{
      return false;
    }
  }

  public boolean delete(int id){
    String[] args = new String[]{Integer.toString(id)};
    long delete=bd.delete(NOMBRE_TABLE_BD,"id=?",args);
    if(delete>0){
      return true;
    }else{
      return false;
    }
  }

  public User query(int id){
    User user=null;
    String[] fields= new String[]{
        "id","name","lastname","birthday","age","sex"
    };
    String[] args = new String[]{Integer.toString(id)};
    Cursor cursor=bd.query(NOMBRE_TABLE_BD,fields,"id=?",args,null,null,null);
    if(cursor.moveToFirst()){
      do{
        user=new User();
        user.setId(cursor.getInt(cursor.getColumnIndex("id")));
        user.setName(cursor.getString(1));
        user.setLastname(cursor.getString(2));
        user.setBirthday(cursor.getString(3));
        user.setAge(cursor.getInt(4));
        user.setSex(cursor.getString(5));
      }while (cursor.moveToNext());
    }
    return user;
  }

  public void close(){
    bd.close();
    baseDatos.close();
  }

}
