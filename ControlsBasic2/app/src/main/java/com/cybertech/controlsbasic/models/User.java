package com.cybertech.controlsbasic.models;

/**
 * Created by Tazmadroid on 26/11/16.
 */

public class User {

  private int id=0;
  private String name=null;
  private String lastname=null;
  private String birthday=null;
  private int age=0;
  private String sex=null;

  public User() {
  }

  public User(int id, String name, String lastname, String birthday, int age, String sex) {
    this.id = id;
    this.name = name;
    this.lastname = lastname;
    this.birthday = birthday;
    this.age = age;
    this.sex = sex;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLastname() {
    return lastname;
  }

  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  public String getBirthday() {
    return birthday;
  }

  public void setBirthday(String birthday) {
    this.birthday = birthday;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public String getSex() {
    return sex;
  }

  public void setSex(String sex) {
    this.sex = sex;
  }
}
