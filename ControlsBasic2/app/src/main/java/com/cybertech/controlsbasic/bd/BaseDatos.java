package com.cybertech.controlsbasic.bd;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Tazmadroid on 26/11/16.
 */

public class BaseDatos extends SQLiteOpenHelper {


  private Context context=null;

  private static final String QUERY_USER="CREATE TABLE users(id INTEGER," +
      "name VARCHAR(100),lastname VARCHAR(100),birthday VARCHAR(20)," +
      "age INTEGER,sex VARCHAR(1))";
  private static final String QUERY_USER_DELETE="DROP TABLE IF EXISTS users";

  public BaseDatos(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
    super(context, name, factory, version);
    this.context=context;
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
    db.execSQL(QUERY_USER);
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    db.execSQL(QUERY_USER_DELETE);
    db.execSQL(QUERY_USER);
  }
}
