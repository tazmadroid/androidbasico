package com.curso.tazmadroid.webviewexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

public class MainActivity extends AppCompatActivity {

  private WebView navigatorWebView = null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    navigatorWebView = (WebView) findViewById(R.id.navigatorWebView);
    navigatorWebView.getSettings().setJavaScriptEnabled(true);
    navigatorWebView.setWebChromeClient(new WebChromeClient());
    navigatorWebView.setWebViewClient(new WebClient());
    navigatorWebView.loadUrl("http://www.aliada.mx");
  }
}
