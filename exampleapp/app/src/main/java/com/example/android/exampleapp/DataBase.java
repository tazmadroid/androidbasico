package com.example.android.exampleapp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by ANDROID on 15/03/2017.
 */

public class DataBase extends SQLiteOpenHelper {

    private Context context=null;
    private static  final String QUERY_USER="CREATE TABLE users(id INTEGER," +
            "name VARCHAR(100), lastname VARCHAR(100), email VARCHAR(100))";
    private static  final String QUERY_USER_DELETE="DROP TABLE IF EXISTS users";

    public DataBase(Context context, String name, SQLiteDatabase.CursorFactory factory,
                    int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(QUERY_USER);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(QUERY_USER_DELETE);
        sqLiteDatabase.execSQL(QUERY_USER);
    }
}
