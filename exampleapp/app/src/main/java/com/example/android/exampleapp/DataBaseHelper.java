package com.example.android.exampleapp;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ANDROID on 15/03/2017.
 */

public class DataBaseHelper {

    private Context context = null;
    private SQLiteDatabase sqLiteDatabase = null;
    private DataBase database = null;

    private static final String DATABASE_NAME = "users_bd";
    private static final int VERSION_BD = 1;

    public DataBaseHelper() {
    }

    public DataBaseHelper(Context context) {
        this.context = context;
        database = new DataBase(context, DATABASE_NAME, null, VERSION_BD);
        sqLiteDatabase = database.getWritableDatabase();
    }

    public boolean insert(Persona persona) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", persona.getId());
        contentValues.put("name", persona.getName());
        contentValues.put("lastname", persona.getLastName());
        contentValues.put("email", persona.getEmail());
        long result = sqLiteDatabase.insert("users", null, contentValues);
        if (result != 0 && result > 0) {
            return true;
        } else {
            return false;
        }
        //sqLiteDatabase.execSQL("insert into users values("+persona.getId()+"");
    }

    public boolean delete(Persona persona) {
        String[] args = new String[]{Integer.toString(persona.getId())};
        long result = sqLiteDatabase.delete("users", "id=?", args);
        if (result != 0 && result > 0)
            return true;
        else
            return false;
    }

    public boolean update(int id, HashMap<String, String> fields) {
        if (fields != null && !fields.isEmpty()) {
            ContentValues contentValues = new ContentValues();
            for (String key : fields.keySet()) {
                contentValues.put(key, fields.get(key));
            }
            String[] args = new String[]{Integer.toString(id)};
            long result = sqLiteDatabase.update("users", contentValues, "id=?", args);
            if (result != 0 && result > 0)
                return true;
            else
                return false;
        } else
            return false;
    }

    public List<Persona> queryAll() {
        List<Persona> personas = null;
        String[] fields = new String[]{
                "id", "name", "lastname", "email"
        };
        Cursor cursor = sqLiteDatabase.query("users", fields, null, null, null, null, null);
        personas=new ArrayList<>();
        if(cursor.moveToFirst()) {
            do {
                personas.add(new Persona(cursor.getInt(0), cursor.getString(1),
                        cursor.getString(2), cursor.getString(3)));
                /*personas.add(new Persona(cursor.getInt(cursor.getColumnIndex("id")),
                        cursor.getString(cursor.getColumnIndex("name")),
                        cursor.getString(cursor.getColumnIndex("lastname")),
                        cursor.getString(cursor.getColumnIndex("email"))));*/
            } while (cursor.moveToNext());
        }
        return personas;
    }

    public Persona query(int id) {
        Persona persona = null;
        String[] args = new String[]{Integer.toString(id)};
        String[] fields = new String[]{
                "id", "name", "lastname", "email"
        };
        Cursor cursor = sqLiteDatabase.query("users", fields, "id=?", args, null, null, null);
        if(cursor.moveToFirst()) {
            do {
                persona=new Persona(cursor.getInt(0), cursor.getString(1),
                        cursor.getString(2), cursor.getString(3));
            } while (cursor.moveToNext());
        }
        return persona;
    }

    public Persona queryLast() {
        Persona persona = null;;
        String[] fields = new String[]{
                "id", "name", "lastname", "email"
        };
        Cursor cursor = sqLiteDatabase.query("users", fields, null, null, null, null, null);
        if(cursor.moveToLast()) {
            do {
                persona=new Persona(cursor.getInt(0), cursor.getString(1),
                        cursor.getString(2), cursor.getString(3));
            } while (cursor.moveToNext());
        }
        return persona;
    }

    public void close(){
        sqLiteDatabase.close();
        database.close();
    }
}
