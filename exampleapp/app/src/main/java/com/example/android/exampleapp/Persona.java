package com.example.android.exampleapp;

/**
 * Created by ANDROID on 16/03/2017.
 */

public class Persona {

    private int id=0;
    private String name=null;
    private String lastName=null;
    private String email=null;

    public Persona() {
    }

    public Persona(int id, String name, String lastName, String email) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
