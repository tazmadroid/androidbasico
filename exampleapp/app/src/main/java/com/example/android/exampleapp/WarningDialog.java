package com.example.android.exampleapp;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

/**
 * Created by ANDROID on 15/03/2017.
 */
public class WarningDialog extends DialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
       /* builder.setMessage("Esto es un mensaje de aviso")
                .setTitle("Aviso")
                .setPositiveButton("OK", new DialogInterface.
                OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        })
        .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });*/
        final String[] states = getResources().getStringArray(R.array.state);
        /*builder.setTitle("Estados").
                setItems(states, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Log.d("Estado seleccionado",states[i]);
                    }
                });*/

       /* builder.setTitle("Estados").
                setSingleChoiceItems(states, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Log.d("Estado seleccionado",states[i]);
                    }
                });*/

        /*builder.setTitle("Estados").
                setMultiChoiceItems(states, null, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i, boolean b) {
                        Log.d("Estado seleccionado",states[i]);
                    }
                });*/
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View sessionView = inflater.inflate(R.layout.session_dialog,null);
        sessionView.findViewById(R.id.initSessionButton).
                setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        builder.setView(sessionView);
        return builder.create();
    }
}
