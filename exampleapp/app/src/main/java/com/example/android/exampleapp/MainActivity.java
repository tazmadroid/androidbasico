package com.example.android.exampleapp;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private TextInputLayout nameTextInputLayout=null;
    private TextInputLayout lastNameTextInputLayout=null;
    private TextInputLayout ageTextInputLayout=null;
    private TextInputLayout addressTextInputLayout=null;
    private TextInputLayout countryTextInputLayout=null;
    private TextInputLayout emailTextInputLayout=null;
    private TextInputLayout numberPhoneTextInputLayout=null;
    private RadioGroup sexRadioGroup=null;
    private Spinner stateSpinner=null;
    private CheckBox musicCheckBox=null;
    private CheckBox booksCheckBox=null;
    private CheckBox technologyCheckBox=null;
    private CheckBox carsCheckBox=null;
    private Button registerButton=null;

    private char sex='M';
    private String state=null;
    private List<String> intereses= new ArrayList<>();
    private DataBaseHelper dataBaseHelper=null;
    private int widgetId=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
       /* SharedPreferences preferences = PreferenceManager.
                getDefaultSharedPreferences(MainActivity.this);
        String user= preferences.getString("example_text","S/U");*/
        Bundle widgetExtra = getIntent().getExtras();
        if(widgetExtra!=null){
            widgetId = widgetExtra.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID);
        }
        init();
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Intent detailIntent = new Intent(MainActivity.this,DetailActivity.class);
                startActivity(detailIntent);
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==DetailActivity.RESULT_MESSAGE){
            if(requestCode==DetailActivity.RESULT_MESSAGE)
                Toast.makeText(getBaseContext(),"Se actualizo la informacion",
                        Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onDestroy() {
        if(dataBaseHelper!=null)
            dataBaseHelper.close();
        super.onDestroy();
}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void init(){
        dataBaseHelper = new DataBaseHelper(getBaseContext());
        nameTextInputLayout = (TextInputLayout) findViewById(R.id.nameTextInputLayout);
        nameTextInputLayout.getEditText().addTextChangedListener(nameWatcher);
        lastNameTextInputLayout = (TextInputLayout) findViewById(R.id.lastNameTextInputLayout);
        ageTextInputLayout = (TextInputLayout) findViewById(R.id.ageTextInputLayout);
        addressTextInputLayout= (TextInputLayout) findViewById(R.id.addressTextInputLayout);
        countryTextInputLayout= (TextInputLayout) findViewById(R.id.countryTextInputLayout);
        emailTextInputLayout = (TextInputLayout) findViewById(R.id.emailTextInputLayout);
        numberPhoneTextInputLayout = (TextInputLayout) findViewById(R.id.numberPhoneTextInputLayout);
        sexRadioGroup = (RadioGroup) findViewById(R.id.sexRadioGroup);
        stateSpinner = (Spinner) findViewById(R.id.stateSpinner);
        musicCheckBox = (CheckBox) findViewById(R.id.musicCheckBox);
        booksCheckBox = (CheckBox) findViewById(R.id.booksCheckBox);
        technologyCheckBox = (CheckBox) findViewById(R.id.technologyCheckBox);
        carsCheckBox = (CheckBox) findViewById(R.id.carsCheckBox);
        registerButton = (Button) findViewById(R.id.registerButton);

        sexRadioGroup.setOnCheckedChangeListener(sexOnCheckedChangeListener);
        initStateSpinner();
        musicCheckBox.setOnCheckedChangeListener(musicaOnCheckedChangeListener);
    }

    private void initStateSpinner(){
        final ArrayAdapter<CharSequence> stateAdapter = ArrayAdapter.
                createFromResource(getBaseContext(),R.array.state,
                        android.R.layout.simple_spinner_item);
        stateSpinner.setAdapter(stateAdapter);
        stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                state=stateAdapter.getItem(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                state="";
            }
        });
    }

    private RadioGroup.OnCheckedChangeListener sexOnCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int i) {
            if(radioGroup.getCheckedRadioButtonId()==R.id.maleRadioButton){
                sex='M';
            }else if(radioGroup.getCheckedRadioButtonId()==R.id.femaleRadioButton){
                sex='F';
            }else{
                sex='M';
            }
        }
    };

    private TextWatcher nameWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if(charSequence.toString().contains("2")){
                nameTextInputLayout.setErrorEnabled(true);
                nameTextInputLayout.setError("No se aceptan numeros");
            }else{
                nameTextInputLayout.setErrorEnabled(false);
                nameTextInputLayout.setError("");
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    private CompoundButton.OnCheckedChangeListener musicaOnCheckedChangeListener = new
            CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(b)
                        intereses.add("Musica");
                    else{
                        if(intereses.contains("Musica"))
                            intereses.remove("Musica");
                    }
                }
            };

    private void registerUser(){
        String name = nameTextInputLayout.getEditText().getText().toString();
        String lastName = lastNameTextInputLayout.getEditText().getText().toString();
        String age = ageTextInputLayout.getEditText().getText().toString();
        String address= addressTextInputLayout.getEditText().getText().toString();
        String country= countryTextInputLayout.getEditText().getText().toString();
        String email= emailTextInputLayout.getEditText().getText().toString();
        String numberPhone = numberPhoneTextInputLayout.getEditText().getText().toString();



        if(intereses.isEmpty()) {
            Log.d("[Usuario]", name + "/" + lastName + "/" + "/" + age + "/" + address +
                    "/" + country + "/" + email + "/" + numberPhone + "/" + sex + "/" + state);
        }else{
            for(String interes : intereses){
                Log.d("[Usuario]", name + "/" + lastName + "/" + "/" + age + "/" + address +
                        "/" + country + "/" + email + "/" + numberPhone + "/" + sex + "/" + state+
                        "/"+interes);
            }
        }
        if(name.isEmpty()
                || lastName.isEmpty()
                ||  age.isEmpty() ||  email.isEmpty()){
            Intent noIntent=new Intent(MainActivity.this,MainActivity.class);

            PendingIntent pendingIntent = PendingIntent.
                    getActivity(MainActivity.this,0,noIntent,0);

            NotificationCompat.Builder notification = new NotificationCompat.
                    Builder(MainActivity.this).setSmallIcon(R.mipmap.ic_launcher).
                    setContentTitle("Aviso").setContentText("Es un aviso de notificacón").
                    setTicker("Alerta").setContentIntent(pendingIntent);

            NotificationManager notificationManager = (NotificationManager)
                    getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(54454,notification.build());
        }else{
            Persona persona = new Persona(name.length(),name,lastName,email);
            if(dataBaseHelper.insert(persona)) {
                //savePersona(persona);
                //Intent detailIntent = new Intent(this, SettingsActivity.class);
               /* Bundle detailBundle = new Bundle();

                detailBundle.putInt("id",name.length());
                detailIntent.putExtras(detailBundle);*/
                //setResult(RESULT_OK);
                //startActivity(detailIntent);
                updateWidget(persona.getId());
            }else
                Toast.makeText(getBaseContext(),"No se registro la persona",Toast.LENGTH_LONG).
                        show();
        }
    }

    public void onRegisterClick(View view){
       registerUser();
    }

    public void savePersona(Persona persona){
        SharedPreferences preferences =getSharedPreferences("personaPreference",MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("id",persona.getId());
        editor.putString("name",persona.getName());
        editor.putString("lastname",persona.getLastName());
        editor.putString("email",persona.getEmail());
        editor.commit();
    }

    public void updateWidget(int id){
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(MainActivity.this);
        ContactWidgetProvider.updateWidget(MainActivity.this,appWidgetManager,widgetId,id);
        Intent resultado = new Intent();
        resultado.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,widgetId);
        setResult(RESULT_OK,resultado);
        finish();
    }
}
