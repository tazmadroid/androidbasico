package com.example.android.exampleapp;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

/**
 * Created by ANDROID on 21/03/2017.
 */

public class ContactWidgetProvider extends AppWidgetProvider {

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals("com.example.android.exampleapp.UPDATE_WIDGET")){
            int widgetId=intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID);
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);

            if(widgetId!=AppWidgetManager.INVALID_APPWIDGET_ID){
                updateWidget(context,appWidgetManager,widgetId);
            }
        }
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for(int i=0;i<appWidgetIds.length;i++){
            int widgetId=appWidgetIds[i];
            updateWidget(context,appWidgetManager,widgetId);
        }
    }

    public static void updateWidget(Context context, AppWidgetManager appWidgetManager,
                                    int widgetId){
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        Persona persona = dataBaseHelper.queryLast();
        RemoteViews views = new RemoteViews(context.getPackageName(),R.layout.contact_widget_view);

        if(persona!=null){
            views.setTextViewText(R.id.nameWidgetTextView,persona.getName());
            views.setTextViewText(R.id.lastnameWidgetTextView,persona.getLastName());
            views.setTextViewText(R.id.emailWidgetTextView,persona.getEmail());
        }

        Intent widgetIntent = new Intent("com.example.android.exampleapp.UPDATE_WIDGET");
        widgetIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,widgetId);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,widgetId,widgetIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        views.setOnClickPendingIntent(R.id.updateWidgetButton,pendingIntent);
        appWidgetManager.updateAppWidget(widgetId,views);
        dataBaseHelper.close();
    }

    public static void updateWidget(Context context, AppWidgetManager appWidgetManager,
                                    int widgetId,int idPersona){
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        Persona persona = dataBaseHelper.query(idPersona);
        RemoteViews views = new RemoteViews(context.getPackageName(),R.layout.contact_widget_view);

        if(persona!=null){
            views.setTextViewText(R.id.nameWidgetTextView,persona.getName());
            views.setTextViewText(R.id.lastnameWidgetTextView,persona.getLastName());
            views.setTextViewText(R.id.emailWidgetTextView,persona.getEmail());
        }

        Intent widgetIntent = new Intent("com.example.android.exampleapp.UPDATE_WIDGET");
        widgetIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,widgetId);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,widgetId,widgetIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        views.setOnClickPendingIntent(R.id.updateWidgetButton,pendingIntent);
        appWidgetManager.updateAppWidget(widgetId,views);
        dataBaseHelper.close();
    }
}
