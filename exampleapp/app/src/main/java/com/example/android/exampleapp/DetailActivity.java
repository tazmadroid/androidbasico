package com.example.android.exampleapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    private TextView nameTextView = null;
    private TextView lastNameTextView = null;
    private TextView edadTextView = null;
    private Button updateButton = null;

    public static final int RESULT_MESSAGE = 1254;

    private DataBaseHelper dataBaseHelper=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        //dataBaseHelper = new DataBaseHelper(getBaseContext());
        Intent detailIntent = getIntent();
        Bundle extras = detailIntent.getExtras();
        //Persona persona = dataBaseHelper.query(extras.getInt("id"));
        SharedPreferences preferences =getSharedPreferences("personaPreference",MODE_PRIVATE);
        Persona persona = new Persona();
        persona.setId(preferences.getInt("id",0));
        persona.setName(preferences.getString("name","S/N"));
        persona.setLastName(preferences.getString("lastname","S/A"));
        persona.setEmail(preferences.getString("email","S/CE"));
        nameTextView = (TextView) findViewById(R.id.nameTextView);
        lastNameTextView = (TextView) findViewById(R.id.lastNameTextView);
        edadTextView = (TextView) findViewById(R.id.edadTextView);
        updateButton = (Button) findViewById(R.id.updateButton);

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WarningDialog warningDialog = new WarningDialog();
                warningDialog.show(getSupportFragmentManager(), "WARNING");
            }
        });

        if(persona!=null) {
            nameTextView.setText(persona.getName());
            lastNameTextView.setText(persona.getLastName());
            edadTextView.setText(persona.getEmail());
        }
    }

    @Override
    protected void onDestroy() {
       // if(dataBaseHelper!=null)
          //  dataBaseHelper.close();
        super.onDestroy();
    }
}
