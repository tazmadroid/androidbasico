package com.cybertech.basedatosapp;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.cybertech.basedatosapp.databases.BDHelper;
import com.cybertech.basedatosapp.model.User;

/**
 * BaseDatosApp
 * com.cybertech.basedatosapp
 * <p/>
 * Creado por:I.S.C. Jaive Torres Pineda .
 * Descripción:
 * Fecha de creación: 16/11/16.
 * Última actualización:16/11/16.
 * Actualizado por:
 */
public class ContactWidgetProvider extends AppWidgetProvider {

  @Override
  public void onReceive(Context context, Intent intent) {
    if(intent.getAction().equals("com.cybertech.basedatosapp.UPDATE_WIDGET")){
      int widgetId = intent.getIntExtra(
          AppWidgetManager.EXTRA_APPWIDGET_ID,
          AppWidgetManager.INVALID_APPWIDGET_ID);

      AppWidgetManager widgetManager =
          AppWidgetManager.getInstance(context);

      if (widgetId != AppWidgetManager.INVALID_APPWIDGET_ID) {
        updateWidget(context, widgetManager, widgetId);
      }
    }
  }

  @Override
  public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
    for (int i = 0; i < appWidgetIds.length; i++)
    {
      int widgetId = appWidgetIds[i];

      updateWidget(context, appWidgetManager, widgetId);
    }
  }

  public static void updateWidget(Context context,
                                  AppWidgetManager appWidgetManager,int widgetId){
    BDHelper bdHelper=new BDHelper(context);
    User user = bdHelper.query();
    RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.contact_widget_view);

    if(user!=null) {
      views.setTextViewText(R.id.nameTextView, user.getNombre());
      views.setTextViewText(R.id.lastNameTextView, user.getLastName());
      views.setTextViewText(R.id.birthdayTextView, user.getBirthday());
    }

    Intent intent = new Intent("com.cybertech.basedatosapp.UPDATE_WIDGET");
    Intent intentMain = new Intent(context,MainActivity.class);
    intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,widgetId);
    PendingIntent pendingIntent = PendingIntent.getBroadcast(context,widgetId,
        intent,PendingIntent.FLAG_UPDATE_CURRENT);
    PendingIntent pendingIntent2 = PendingIntent.getBroadcast(context,widgetId,
        intentMain,PendingIntent.FLAG_UPDATE_CURRENT);
    views.setOnClickPendingIntent(R.id.updateImageButton,pendingIntent);
    views.setOnClickPendingIntent(R.id.container_widget,pendingIntent2);
    appWidgetManager.updateAppWidget(widgetId, views);
    bdHelper.close();
  }

  public static void updateWidget(Context context,
                                  AppWidgetManager appWidgetManager,int widgetId, int idUser){
    BDHelper bdHelper=new BDHelper(context);
    User user = bdHelper.query(idUser);
    RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.contact_widget_view);

    if(user!=null) {
      views.setTextViewText(R.id.nameTextView, user.getNombre());
      views.setTextViewText(R.id.lastNameTextView, user.getLastName());
      views.setTextViewText(R.id.birthdayTextView, user.getBirthday());
    }

    Intent intent = new Intent("com.cybertech.basedatosapp.UPDATE_WIDGET");
    intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,widgetId);
    PendingIntent pendingIntent = PendingIntent.getBroadcast(context,widgetId,
        intent,PendingIntent.FLAG_UPDATE_CURRENT);
    views.setOnClickPendingIntent(R.id.updateImageButton,pendingIntent);
    appWidgetManager.updateAppWidget(widgetId, views);
    bdHelper.close();
  }
}
