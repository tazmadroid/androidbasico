package com.cybertech.basedatosapp;

import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.cybertech.basedatosapp.databases.BDHelper;
import com.cybertech.basedatosapp.model.User;

public class MainActivity extends AppCompatActivity {

  private TextInputLayout nameTextInputLayout=null;
  private TextInputLayout lastNameTextInputLayout=null;
  private TextInputLayout birthdayTextInputLayout=null;
  private TextInputLayout ageTextInputLayout=null;
  private TextInputLayout sexTextInputLayout=null;
  private Button saveButton=null;

  private BDHelper bdHelper = null;

  private int widgetId=0;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
//  Es para el ejemplo del widget
    Bundle widgetExtras=getIntent().getExtras();
    if(widgetExtras!=null) {
      widgetId = widgetExtras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID,
          AppWidgetManager.INVALID_APPWIDGET_ID);
    }
    setResult(RESULT_CANCELED);

    bdHelper = new BDHelper(getBaseContext());
    nameTextInputLayout = (TextInputLayout) findViewById(R.id.nameTextInputLayout);
    lastNameTextInputLayout = (TextInputLayout) findViewById(R.id.lastnameTextInputLayout);
    birthdayTextInputLayout = (TextInputLayout) findViewById(R.id.birthdayTextInputLayout);
    ageTextInputLayout = (TextInputLayout) findViewById(R.id.ageTextInputLayout);
    sexTextInputLayout = (TextInputLayout) findViewById(R.id.sexTextInputLayout);
    saveButton = (Button) findViewById(R.id.saveButton);

    nameTextInputLayout.setErrorEnabled(true);
    lastNameTextInputLayout.setErrorEnabled(true);
    birthdayTextInputLayout.setErrorEnabled(true);
    ageTextInputLayout.setErrorEnabled(true);
    sexTextInputLayout.setErrorEnabled(true);

    saveButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        String name=nameTextInputLayout.getEditText().getText().toString();
        String lastName=lastNameTextInputLayout.getEditText().getText().toString();
        String birthday=birthdayTextInputLayout.getEditText().getText().toString();
        String age=ageTextInputLayout.getEditText().getText().toString();
        String sex=sexTextInputLayout.getEditText().getText().toString();

        if(name.isEmpty()){
          nameTextInputLayout.setError("El nombre no es válido");
        }else{
          nameTextInputLayout.setError(null);
        }

        if(lastName.isEmpty()){
          lastNameTextInputLayout.setError("El nombre no es válido");
        }else{
          lastNameTextInputLayout.setError(null);
        }
        User user = new User(name.hashCode(),name,lastName,birthday,age,sex);
        if(bdHelper.insert(user)) {
          Toast.makeText(getBaseContext(), "El usuario se registro", Toast.LENGTH_SHORT).show();
//        Es para el ejemplo del widget
          updateWidget(user.getId());
        }else
          Toast.makeText(getBaseContext(),"El usuario no se registro",Toast.LENGTH_SHORT).show();
      }
    });
  }

  @Override
  protected void onDestroy() {
    bdHelper.close();
    super.onDestroy();
  }

  private void updateWidget(int userId){
    AppWidgetManager appWidgetManager =
        AppWidgetManager.getInstance(MainActivity.this);
    ContactWidgetProvider.updateWidget(MainActivity.this, appWidgetManager,
        widgetId,userId);
    Intent resultado = new Intent();
    resultado.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);
    setResult(RESULT_OK, resultado);
    finish();
  }
}
