package com.cybertech.basedatosapp.databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.cybertech.basedatosapp.model.User;

import java.util.HashMap;

/**
 * BaseDatosApp
 * com.cybertech.basedatosapp.databases
 * <p/>
 * Creado por:I.S.C. Jaive Torres Pineda .
 * Descripción:
 * Fecha de creación: 11/11/16.
 * Última actualización:11/11/16.
 * Actualizado por:
 */
public class BDHelper {

  private Context context = null;
  private SQLiteDatabase sqLiteDatabase = null;
  private Database database = null;

  private static final String DATABASE_NAME = "users_bd";
  private static final int VERSION_BD = 1;

  public BDHelper() {
  }

  public BDHelper(Context context) {
    this.context = context;
    database = new Database(context, DATABASE_NAME, null, VERSION_BD);
    sqLiteDatabase = database.getWritableDatabase();
  }

  public boolean insert(User user) {
    ContentValues contentValues = new ContentValues();
    contentValues.put("id", user.getId());
    contentValues.put("name", user.getNombre());
    contentValues.put("lastname", user.getLastName());
    contentValues.put("birthday", user.getBirthday());
    contentValues.put("age", user.getAge());
    contentValues.put("sex", user.getSex());
    long insert = sqLiteDatabase.insert("users", null, contentValues);
    if (insert != 0 && insert > 0)
      return true;
    else {
      return false;
    }
  }

  public boolean update(int id, HashMap<String, String> fields) {
    if (fields != null && fields.size() != 0) {
      ContentValues contentValues = new ContentValues();
      for (String key : fields.keySet()) {
        contentValues.put(key, fields.get(key));
      }
      String[] args = new String[]{Integer.toString(id)};
      long update = sqLiteDatabase.update("users", contentValues, "id=?", args);
      if (update != 0 && update > 0)
        return true;
      else
        return false;
    } else
      return false;
  }

  public boolean delete(int id) {
    String[] args = new String[]{Integer.toString(id)};
    long delete = sqLiteDatabase.delete("users", "id=?", args);
    if (delete != 0 && delete > 0)
      return true;
    else
      return false;
  }

  public User query(int id){
    User user=null;
    String[] fields= new String[]{
      "id","name","lastname","birthday","age","sex"
    };
    String[] args=new String[]{Integer.toString(id)};
    Cursor cursor=sqLiteDatabase.query("users",fields,"id=?",args,null,null,null);
    if(cursor.moveToFirst()){
      do{
        user=new User();
        user.setId(cursor.getInt(0));
        user.setNombre(cursor.getString(1));
        user.setLastName(cursor.getString(2));
        user.setBirthday(cursor.getString(3));
        user.setAge(cursor.getString(4));
        user.setSex(cursor.getString(5));
      }while (cursor.moveToNext());
    }
    return user;
  }

  public User query(){
    User user=null;
    String[] fields= new String[]{
      "id","name","lastname","birthday","age","sex"
    };
    Cursor cursor=sqLiteDatabase.query("users",fields,null,null,null,null,null);
    if(cursor.moveToLast()){
      do{
        user=new User();
        user.setId(cursor.getInt(0));
        user.setNombre(cursor.getString(1));
        user.setLastName(cursor.getString(2));
        user.setBirthday(cursor.getString(3));
        user.setAge(cursor.getString(4));
        user.setSex(cursor.getString(5));
      }while (cursor.moveToNext());
    }
    return user;
  }



  public void close(){
    sqLiteDatabase.close();
    database.close();
  }
}
