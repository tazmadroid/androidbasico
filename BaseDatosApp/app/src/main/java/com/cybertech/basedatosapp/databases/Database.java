package com.cybertech.basedatosapp.databases;

import android.annotation.TargetApi;
import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;

/**
 * BaseDatosApp
 * com.cybertech.basedatosapp.databases
 * <p/>
 * Creado por:I.S.C. Jaive Torres Pineda .
 * Descripción:
 * Fecha de creación: 11/11/16.
 * Última actualización:11/11/16.
 * Actualizado por:
 */
public class Database extends SQLiteOpenHelper {

  private Context context=null;
  private static final String QUERY_USER="CREATE TABLE users(id INTEGER," +
      "name VARCHAR(100),lastname VARCHAR(100),birthday VARCHAR(20)," +
      "age INTEGER,sex VARCHAR(1))";
  private static final String QUERY_USER_DELETE="DROP TABLE IF EXISTS users";

  public Database(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
    super(context, name, factory, version);
    this.context=context;
  }

  @TargetApi(Build.VERSION_CODES.HONEYCOMB)
  public Database(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
    super(context, name, factory, version, errorHandler);
  }

  @Override
  public void onCreate(SQLiteDatabase sqLiteDatabase) {
    sqLiteDatabase.execSQL(QUERY_USER);
  }

  @Override
  public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
    sqLiteDatabase.execSQL(QUERY_USER_DELETE);
    sqLiteDatabase.execSQL(QUERY_USER);
  }
}
