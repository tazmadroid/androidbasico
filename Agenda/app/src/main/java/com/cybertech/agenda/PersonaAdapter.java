package com.cybertech.agenda;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by ANDROID on 24/03/2017.
 */

public class PersonaAdapter extends RecyclerView.Adapter<PersonaAdapter.PersonaViewHolder> {

    private Context context = null;
    private List<Persona> personas=null;

    public PersonaAdapter(Context context, List<Persona> personas){
        this.context=context;
        this.personas=personas;
    }

    @Override
    public PersonaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.
                item_persona_cardview, parent,false);
        PersonaViewHolder personaViewHolder = new PersonaViewHolder(itemView);
        return personaViewHolder;
    }

    @Override
    public void onBindViewHolder(PersonaViewHolder holder, int position) {
        holder.bindItemPersona(personas.get(position));
    }

    @Override
    public int getItemCount() {
        return personas.size();
    }

    public static class PersonaViewHolder extends RecyclerView.ViewHolder{

        private TextView nameTextView=null;
        private TextView lastNameTextView=null;
        private TextView emailTextView=null;
        private TextView categoryTextView=null;
        private ImageButton callImageButton=null;
        private ImageButton emailImageButton=null;

        public PersonaViewHolder(View itemView) {
            super(itemView);
            nameTextView = (TextView) itemView.findViewById(R.id.nameTextView);
            lastNameTextView = (TextView) itemView.findViewById(R.id.lastNameTextView);
            emailTextView = (TextView) itemView.findViewById(R.id.emailTextView);
            categoryTextView = (TextView) itemView.findViewById(R.id.categoryTextView);
            callImageButton = (ImageButton) itemView.findViewById(R.id.callImageButton);
            emailImageButton = (ImageButton) itemView.findViewById(R.id.emailImageButton);
        }

        public void bindItemPersona(final Persona persona){
            nameTextView.setText(persona.getName());
            lastNameTextView.setText(persona.getLastName());
            emailTextView.setText(persona.getEmail());
            categoryTextView.setText(persona.getCategoria());
            emailImageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("[EMAIL]",persona.getEmail());
                }
            });
        }
    }
}
