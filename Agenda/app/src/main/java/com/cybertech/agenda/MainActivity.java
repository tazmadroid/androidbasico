package com.cybertech.agenda;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.StandaloneActionMode;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView personasRecyclerView=null;
    private PersonaAdapter personaAdapter=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        personasRecyclerView = (RecyclerView) findViewById(R.id.personasRecyclerView);
        init();
    }

    private void init(){
        personasRecyclerView.setHasFixedSize(true);
        /*StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL);
        GridLayoutManager layoutManager = new GridLayoutManager(getBaseContext(),2);*/
        LinearLayoutManager layoutManager = new LinearLayoutManager(getBaseContext(),
                LinearLayoutManager.VERTICAL,false);
        personasRecyclerView.setLayoutManager(layoutManager);
        List<Persona> personaList=new ArrayList<>();
        for(int i=0;i<=20;i++){
           // if(i%2==0)
            personaList.add(new Persona(i,"Jaive","Torres Pineda",
                    "jtpcybertron85@gmail.com"+i,"Familia"));
           /* else
                personaList.add(new Persona(i,"Jaive","Torres Pineda",
                        "jtpcybertron85@gmail.com","Familia"));*/
        }
        personaAdapter=new PersonaAdapter(getBaseContext(),personaList);
        personasRecyclerView.setAdapter(personaAdapter);
    }
}
