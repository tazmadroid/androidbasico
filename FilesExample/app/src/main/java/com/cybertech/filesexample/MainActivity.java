package com.cybertech.filesexample;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;


public class MainActivity extends AppCompatActivity {

    private EditText lineEditText = null;
    private Button internButton = null;
    private Button externButton = null;
    private Button readInternButton = null;
    private Button readExternButton = null;
    private TextView lineTextView = null;


    private static final String NAME_FILE = "personas.txt";
    private static final int PERMISSION_SD = 2452;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            verificarPermisos();
        createFileIntern();
        lineEditText = (EditText) findViewById(R.id.lineEditText);
        internButton = (Button) findViewById(R.id.internButton);
        externButton = (Button) findViewById(R.id.externButton);
        readInternButton = (Button) findViewById(R.id.readInternButton);
        readExternButton = (Button) findViewById(R.id.readExternButton);
        lineTextView = (TextView) findViewById(R.id.lineTextView);

        internButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String line = lineEditText.getText().toString();
                writeFileIntern(line);
            }
        });

        readInternButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String line = readFileIntern();
                lineTextView.setText(line);
            }
        });

        externButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String line = lineEditText.getText().toString();
                writeFileExtern(line);
            }
        });

        readExternButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String line= readFileExtern();
                lineTextView.setText(line);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_SD:
                if (grantResults.length > 0 &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getBaseContext(), "Listo", Toast.LENGTH_LONG).show();
                }
                return;
        }
    }

    private void createFileIntern() {
        try {
            OutputStreamWriter fout = new OutputStreamWriter(openFileOutput(NAME_FILE,
                    MODE_PRIVATE));
            fout.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void writeFileIntern(String line) {
        try {
            OutputStreamWriter fout = new OutputStreamWriter(openFileOutput(NAME_FILE, MODE_APPEND));
            fout.write(line);
            fout.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String readFileIntern() {
        String line = null;
        try {
            BufferedReader fin = new BufferedReader(new
                    InputStreamReader(openFileInput(NAME_FILE)));
            line = fin.readLine();
            fin.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return line;
    }

    private boolean isAccesibleSD(){
        boolean isAvailable=false;
        boolean isWritable=false;
        String stateSD= Environment.getExternalStorageState();
        if(stateSD.equals(Environment.MEDIA_MOUNTED)){
            isAvailable=true;
            isWritable=true;
        } else  if(stateSD.equals(Environment.MEDIA_MOUNTED_READ_ONLY)){
            isAvailable=true;
            isWritable=false;
        }else{
            isAvailable=false;
            isWritable=false;
        }
        return isAvailable && isWritable;
    }

    private void createFileSD(){
        try{
            if(isAccesibleSD()){
                File ruta_sd=Environment.getExternalStorageDirectory();
                File file=new File(ruta_sd,NAME_FILE);

                if(!file.exists()) {
                    OutputStreamWriter fout = new OutputStreamWriter(new FileOutputStream(file));
                    fout.close();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void writeFileExtern(String line){
        try{
            if(isAccesibleSD()){
                File ruta_sd=Environment.getExternalStorageDirectory();
                File file = new File(ruta_sd,NAME_FILE);
                createFileSD();
                OutputStreamWriter fout = new OutputStreamWriter(new FileOutputStream(file));
                fout.write(line);
                fout.close();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private String readFileExtern(){
        String line=null;
        try{
            if(isAccesibleSD()){
                File ruta_sd=Environment.getExternalStorageDirectory();
                File file = new File(ruta_sd,NAME_FILE);

                BufferedReader fin=new BufferedReader(new InputStreamReader(
                        new FileInputStream(file)
                ));
                line=fin.readLine();
                fin.close();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return line;
    }

    private boolean verificarPermisos() {
        boolean hasPermissionSD = (ContextCompat.checkSelfPermission(getBaseContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) ==
                PackageManager.PERMISSION_GRANTED;
        if (!hasPermissionSD) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(MainActivity.this, new
                        String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_SD);
            } else {
                ActivityCompat.requestPermissions(MainActivity.this, new
                        String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_SD);

            }
        }
        return hasPermissionSD;
    }
}
